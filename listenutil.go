// listenutil provides TCP and websocket listeners.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package listenutil

import (
	"context"
	"errors"
	"fmt"
	"io"
	"net"
	"net/http"
	"net/url"
	"nhooyr.io/websocket"
	"strconv"
	"sync"
	"time"
)

// Printer is the interface satisfied by the Printf method.
type Printer interface {
	Printf(format string, v ...interface{})
}

// addr implements a basic net.Addr for a websocket.
type addr struct{}

// WSListener provides a net.Listener.
type WSListener struct {
	doneC    chan struct{}   // The done channel
	connC    <-chan net.Conn // The connection channel
	m        sync.RWMutex    // Controls access to the following
	isClosed bool            // Are we closed?
	err      error           // The close error (if any)
}

// HandlerFunc is a function that can be passed to http.ServMux.HandleFunc to handle a http request.
type HandlerFunc func(http.ResponseWriter, *http.Request)

// shutdownTimeout is the length of time we allow a server for a graceful shutdown.
const shutdownTimeout = 3 * time.Second

// Option sets options on a TCP listener.
type Option interface {
	apply(*serverOptions)
}

// funcOption wraps a function that modifies Options into an implementation of the Option interface.
type funcOption struct {
	f func(*serverOptions)
}

// apply calls the wrapped function f on the given Options.
func (h *funcOption) apply(do *serverOptions) {
	h.f(do)
}

// newFuncOption returns a funcOption wrapping f.
func newFuncOption(f func(*serverOptions)) *funcOption {
	return &funcOption{
		f: f,
	}
}

// serverOptions are the options on a TCP listener.
type serverOptions struct {
	MaxNumConnections int     // The maximum number of connections
	Log               Printer // The logger
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// webserverListener returns a TCP listener, defaulting to port 80.
func webserverListener(u *url.URL, options ...Option) (net.Listener, error) {
	// Recover the address and port number
	address := u.Hostname()
	port := 80
	if portStr := u.Port(); len(portStr) != 0 {
		n, err := strconv.Atoi(portStr)
		if err != nil {
			return nil, fmt.Errorf("unable to parse port number: %w", err)
		}
		port = n
	}
	// Start a listener
	return TCPListener(address, port, options...)
}

/////////////////////////////////////////////////////////////////////////
// addr functions
/////////////////////////////////////////////////////////////////////////

// Network returns "ws"
func (addr) Network() string {
	return "ws"
}

// String returns "ws"
func (addr) String() string {
	return "ws"
}

/////////////////////////////////////////////////////////////////////////
// WSListener functions
/////////////////////////////////////////////////////////////////////////

// Accept waits for and returns the next connection to the listener.
func (l *WSListener) Accept() (net.Conn, error) {
	if l == nil {
		return nil, errors.New("uninitialised WSListener")
	}
	select {
	case conn := <-l.connC:
		return conn, nil
	case <-l.doneC:
		// Acquire a read lock
		l.m.RLock()
		defer l.m.RUnlock()
		// Return an error
		err := l.err
		if err == nil {
			err = io.EOF
		}
		return nil, err
	}
}

// Close closes the listener. Any blocked Accept operations will be unblocked and return errors. Already Accepted connections are not closed.
func (l *WSListener) Close() error {
	return l.CloseWithErr(nil)
}

// CloseWithErr closes the listener as per Close, setting any error err to be returned.
func (l *WSListener) CloseWithErr(err error) error {
	// Sanity check
	if l == nil {
		return nil
	}
	// Acquire a lock
	l.m.Lock()
	defer l.m.Unlock()
	// Is there anything to do?
	if !l.isClosed {
		l.isClosed = true
		l.err = err
		close(l.doneC)
	}
	return l.err
}

// Addr returns a dummy address whose Network() and String() values are always "ws".
func (*WSListener) Addr() net.Addr {
	return addr{}
}

/////////////////////////////////////////////////////////////////////////
// Options functions
/////////////////////////////////////////////////////////////////////////

// parseOptions parses the given optional functions.
func parseOptions(options ...Option) *serverOptions {
	// Create the default options
	opts := &serverOptions{}
	// Set the options
	for _, h := range options {
		h.apply(opts)
	}
	return opts
}

// Log sets the logger.
func Log(lg Printer) Option {
	return newFuncOption(func(opts *serverOptions) {
		opts.Log = lg
	})
}

// MaxNumConnections sets the maximum number of connections.
func MaxNumConnections(n int) Option {
	if n < 0 {
		n = 0
	}
	return newFuncOption(func(opts *serverOptions) {
		opts.MaxNumConnections = n
	})
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// TCPListener returns a net.Listener listening over TCP on address:port.
func TCPListener(address string, port int, options ...Option) (net.Listener, error) {
	// Parse the options
	opts := parseOptions(options...)
	// Sanity check
	if port < 1 || port > 65535 {
		return nil, fmt.Errorf("invalid port number (%d)", port)
	}
	// Resolve the address
	addr, err := net.ResolveTCPAddr("tcp", address+":"+strconv.Itoa(port))
	if err != nil {
		return nil, fmt.Errorf("unable to resolve address: %w", err)
	}
	// Establish a connection
	var l net.Listener
	l, err = net.ListenTCP("tcp", addr)
	if err != nil {
		return nil, fmt.Errorf("error starting listener: %w", err)
	}
	// Are we limiting the number of connections?
	if opts.MaxNumConnections > 0 {
		l = LimitListenerWithLog(l, opts.MaxNumConnections, opts.Log)
		// Provide some logging feedback
		if opts.Log != nil {
			opts.Log.Printf("Listening on %s (max-num-connections=%d)",
				l.Addr(),
				opts.MaxNumConnections,
			)
		}
	} else if opts.Log != nil {
		opts.Log.Printf("Listening on %s", l.Addr())
	}
	return l, nil
}

// WebsocketListener returns a net.Listener listening over a websocket. Also returns the handler function that should be passed to your http server in order to promote incoming connections to websocket connections.
func WebsocketListener(lg Printer) (*WSListener, HandlerFunc) {
	// Create the communication channels
	doneC, connC := make(chan struct{}), make(chan net.Conn)
	// Create the handler
	handler := func(w http.ResponseWriter, r *http.Request) {
		// Convert this connection to a websocket
		c, err := websocket.Accept(w, r, &websocket.AcceptOptions{
			InsecureSkipVerify: true,
		})
		if err != nil {
			if lg != nil {
				lg.Printf("Error accepting websocket connection: %v", err)
			}
			return
		}
		// Convert the websocket to a net.Conn
		conn := websocket.NetConn(context.Background(), c, websocket.MessageBinary)
		// Pass the net.Conn down the channel
		select {
		case connC <- conn:
		case <-doneC:
			conn.Close() // Ignore any error
		}
	}
	// Return
	return &WSListener{
		doneC: doneC,
		connC: connC,
	}, handler
}

// WebsocketListenAndServe creates and starts a http.Server listening for websocket connections as described by uri. Returns the net.Listener for this server. A shutdown function for the http.Server is returned as the second return value.
func WebsocketListenAndServe(uri string, options ...Option) (*WSListener, func() error, error) {
	// Parse the options
	opts := parseOptions(options...)
	// Parse the uri
	u, err := url.ParseRequestURI(uri)
	if err != nil {
		return nil, func() error { return nil }, err
	} else if u.Scheme != "ws" {
		return nil, func() error { return nil }, errors.New("unsupported scheme: " + u.Scheme)
	}
	// Create the http.Server listener
	ls, err := webserverListener(u, options...)
	if err != nil {
		return nil, func() error { return nil }, err
	}
	// Create the websocket listener
	lg := opts.Log
	l, handler := WebsocketListener(lg)
	// Create the new server MUX and register the handler
	mux := http.NewServeMux()
	mux.HandleFunc(u.EscapedPath(), handler)
	// Create the server
	s := &http.Server{
		Addr:    u.Host,
		Handler: mux,
	}
	// Start the http.Server serving
	go func() {
		if err := s.Serve(ls); err != http.ErrServerClosed {
			l.CloseWithErr(err) // Ignore any error
		} else {
			l.Close() // Ignore any error
		}
	}()
	// Create the shutdown function
	shutdown := func() error {
		if lg != nil {
			lg.Printf("HTTP server shutting down on %s...", u.Host)
		}
		ctx, cancel := context.WithTimeout(context.Background(), shutdownTimeout)
		defer cancel()
		if err := s.Shutdown(ctx); err != nil {
			s.Close()
			if lg != nil {
				lg.Printf("HTTP server shut down with error: %v", err)
			}
		} else if lg != nil {
			lg.Printf("HTTP server shut down")
		}
		return nil
	}
	// Return the data
	return l, shutdown, nil
}
