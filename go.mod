module bitbucket.org/pcastools/listenutil

go 1.13

require (
	github.com/klauspost/compress v1.11.12 // indirect
	github.com/kr/pretty v0.1.0 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	nhooyr.io/websocket v1.8.6
)
