// Limitlistener wraps a net.Listener and returns a net.Listener which will accept at most N simultaneous connections.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package listenutil

import (
	"net"
	"sync/atomic"
)

type limitListener struct {
	net.Listener
	n   int64   // The number of current connections
	max int64   // The maximum number of simultaneous connections allowed
	lg  Printer // The destination log (if any)
}

type closeFunc func() error

type wrapConn struct {
	net.Conn
	close closeFunc // The close function for the wrapped connection
}

/////////////////////////////////////////////////////////////////////////
// wrapConn functions
/////////////////////////////////////////////////////////////////////////

// Close closes the wrapped connection
func (w *wrapConn) Close() error {
	if w == nil || w.close == nil {
		return nil
	}
	return w.close()
}

/////////////////////////////////////////////////////////////////////////
// limitListener functions
/////////////////////////////////////////////////////////////////////////

// Accept waits for and returns the next connection to the listener.
func (L *limitListener) Accept() (net.Conn, error) {
	conn, err := L.Listener.Accept()
	if err != nil {
		return nil, err
	}
	if atomic.LoadInt64(&L.n) >= L.max {
		// Too many connections -- drop this connection
		if L.lg != nil {
			L.lg.Printf("Maximum number of connection exceeded: Dropping connection %s->%s", conn.RemoteAddr(), conn.LocalAddr())
		}
		conn.Close()
	}
	// Increment the counter, wrap up the connection, and return
	atomic.AddInt64(&L.n, 1)
	return &wrapConn{
		Conn: conn,
		close: func() error {
			err := conn.Close()
			atomic.AddInt64(&L.n, -1) // We decrement the counter on close
			return err
		},
	}, nil
}

// LimitListener returns a Listener based on l that accepts at most n simultaneous connections. It will panic if n is negative.
func LimitListener(l net.Listener, n int) net.Listener {
	return LimitListenerWithLog(l, n, nil)
}

// LimitListenerWithLog returns a Listener based on l that accepts at most n simultaneous connections. It will panic if n is negative. If connections are dropped due to the maximum number of connections being exceeded, this will be logged to lg.
func LimitListenerWithLog(l net.Listener, n int, lg Printer) net.Listener {
	// Sanity check
	if n < 0 {
		panic("LimitListener called with a negative number of connections")
	}
	return &limitListener{
		Listener: l,
		max:      int64(n),
	}
}
